class Student
  attr_reader :first_name, :last_name, :courses
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(new_course)
    if new_course.students.include?(self)
    else
      if has_conflict?(new_course)
        raise "you have conflicts with your schedule"
      end
      @courses << new_course #add the new course to the list
      new_course.students << self #updates the list course's of students
    end
  end

  def has_conflict?(new_course)  #check for conflict
    self.courses.any? { |enroll_course| new_course.conflicts_with?(enroll_course) }
  end

  def course_load
    courses_list = Hash.new(0)
    @courses.each { |course| courses_list[course.department] += course.credits }
    courses_list
  end

end
